var mykey = appConfig.TokenKey;//"Twma-gr-i3ek1zhht34D";
//CONFIG.TokenKey;
const projects = appConfig.ProjectList;
var baseURL;
var lastWeekArr = [];
var sucBuildsArr = [];
var allBuildsArr = [];


function InitiateVars(project_name){
  baseURL = "https://gitlab.com/api/v4/projects/"+projects[project_name];
  sucBuildsArr = [0,0,0,0,0,0,0];
  allBuildsArr = [0,0,0,0,0,0,0];
}

function formatDate(vDate){
  var monthNames = [
    "Jan", "Feb", "Mar",
    "Apr", "May", "Jun", "Jul",
    "Aug", "Sep", "Oct",
    "Nov", "Dec"
  ];
  var tday = new Date(vDate);
  var dd = tday.getDate();
  if(dd<10){
    dd = '0'+dd;
  }
    return monthNames[tday.getMonth()] + ' ' +dd+ ' ' +tday.getFullYear();
}
function getLastWeek(){
    var today = new Date();
    var vweekDay = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 6);
    lastWeekArr[0] = formatDate(vweekDay);
    vweekDay = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 5);
    lastWeekArr[1] = formatDate(vweekDay);
    vweekDay = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 4);
    lastWeekArr[2] = formatDate(vweekDay);
    vweekDay = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 3);
    lastWeekArr[3] = formatDate(vweekDay);
    vweekDay = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 2);
    lastWeekArr[4] = formatDate(vweekDay);
    vweekDay = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1);
    lastWeekArr[5] = formatDate(vweekDay);
    lastWeekArr[6] = formatDate(today);
}

function getOverallInfo(project_name){

  var myURL = baseURL+"/pipelines?private_token="+mykey;

  $.ajax({
    url: myURL,
    type: 'GET',
    data: {
      format: 'json'
    },
    success: function(response) {
     console.log(response);
      //list overall statusistics
      var json_obj = response;//parse JSON
      var suc_obj = jQuery.grep(json_obj, function(a){return a.status == "success"});
      var output="<h4>"+project_name+" Overall statistics</h4><ul>";

      output += "<li>Total: "+json_obj.length+" pipelines </li>";
      output += "<li>Successful: "+suc_obj.length+" pipelines </li>";
      var failedlen = json_obj.length - suc_obj.length;
      output += "<li>Failed: " +failedlen+ " pipelines </li>";
      var pratio = suc_obj.length / json_obj.length * 100;
      output += "<li>Success ratio: "+pratio+" % </li>";
      output+="</ul>";
      $('#content').html(output);
      },
    error: function() {
      $('#content').html("There was an error processing your request. Please try again.");
    },
  });
}

function getOnePage(purl){
  $.ajax({
    url: purl,
    type: 'GET',
    data: {
      format: 'json'
    },
    success: function(response) {
      var monNames = [
        0, "Jan", "Feb", "Mar",
        "Apr", "May", "Jun", "Jul",
        "Aug", "Sep", "Oct",
        "Nov", "Dec"
      ];
     //console.log(response);
      //list overall statusistics
      var pageArr = response;//parse JSON
      var nret = 1;
      for(var i=0; i<pageArr.length; i++){
        var pid = pageArr[i]["id"];
        var suc = pageArr[i]["status"];
        //get date information of every pipeline on the page
        $.ajax({
          url: baseURL+"/pipelines/"+pid+"?private_token="+mykey,
          type: 'GET',
          data: {
            format: 'json'
          },
          success: function(response) {
            //list overall statusistics
            console.log(response);
            var retstr = response.created_at;
            var tdate = monNames[parseInt(retstr.substring(5,7))]+' '+retstr.substring(8,10)+' '+retstr.substring(0,4);
            var i;
            var nfound = 0;
            for(i=0; i<lastWeekArr.length; i++){
              if(tdate == lastWeekArr[i]){
                allBuildsArr[i]++;
                console.log("all builds "+i+"="+allBuildsArr[i]);
                nfound = 1;
                if(suc == "success"){
                  sucBuildsArr[i]++;
                }
              }
            }
            drawChart();
            if(!nfound){
              nret = 0;
            }
          },
          error: function() {
            return 0;
          },
        }); //end of get date information

      }
      return nret;
    },
    error: function() {
      return 0;
    },
  });
}


function prepareData(){
  getLastWeek();
  var npage = 1;
  var pageURL = baseURL+"/pipelines?private_token="+mykey+"&page=";
  var rval = 0;
  do {
    var url = pageURL+npage;
    rval = getOnePage(url);
    npage = npage + 1;
  } while(rval)
}

function drawChart(){
var chart = new CanvasJS.Chart("chartContainer", {
animationEnabled: true,
title: {
  text: "Pipelines for Last Week ( "+lastWeekArr[0]+' - '+lastWeekArr[6]+" ) "
},
axisY: {
  title: "All Builds",
  titleFontColor: "#4F81BC",
  lineColor: "#4F81BC",
  labelFontColor: "#4F81BC",
  tickColor: "#4F81BC"
},
axisY2: {
  title: "Successful Builds",
  titleFontColor: "#C0504E",
  lineColor: "#C0504E",
  labelFontColor: "#C0504E",
  tickColor: "#C0504E"
},
toolTip: {
  shared: true
},
legend: {
  cursor:"pointer",
  //itemclick: toggleDataSeries
},
data: [{
  type: "column",
  name: "All Builds",
  legendText: "All Builds",
  showInLegend: true,
  dataPoints:[
      { label: lastWeekArr[0],  y: allBuildsArr[0] },
			{ label: lastWeekArr[1],  y: allBuildsArr[1] },
			{ label: lastWeekArr[2],  y: allBuildsArr[2] },
			{ label: lastWeekArr[3],  y: allBuildsArr[3] },
      { label: lastWeekArr[4],  y: allBuildsArr[4] },
			{ label: lastWeekArr[5],  y: allBuildsArr[5] },
			{ label: lastWeekArr[6],  y: allBuildsArr[6] }
  ]
},
{
  type: "column",
  name: "Successful Builds",
  legendText: "Successful Builds",
  axisYType: "secondary",
  showInLegend: true,
  dataPoints:[
      { label: lastWeekArr[0],  y: sucBuildsArr[0] },
      { label: lastWeekArr[1],  y: sucBuildsArr[1] },
      { label: lastWeekArr[2],  y: sucBuildsArr[2] },
      { label: lastWeekArr[3],  y: sucBuildsArr[3] },
      { label: lastWeekArr[4],  y: sucBuildsArr[4] },
      { label: lastWeekArr[5],  y: sucBuildsArr[5] },
      { label: lastWeekArr[6],  y: sucBuildsArr[6] }
  ]
}]
});

chart.render();
/*
  function toggleDataSeries(e) {
  	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
  		e.dataSeries.visible = false;
  	}
  	else {
  		e.dataSeries.visible = true;
  	}
  	chart.render();
  }
*/
}


//body of main function
$(document).ready(function() {

  $("a").click(function(e) {
    e.preventDefault();
    var pname = $(this).attr("href");
    InitiateVars(pname);
    getOverallInfo(pname);
    prepareData();

    //draw chart
    //drawChart();

  })
});
